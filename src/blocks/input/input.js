if ($(".input--calendar")[0]){
  $(function() {
    $('.input--calendar').datepicker({
      yearRange: "-100:+3",
      defaultDate: null,
      changeMonth: true,
      changeYear: true,
      dateFormat: "dd.mm.yy",
      numberOfMonths: 1,
      firstDay: 1
    });
  });
}