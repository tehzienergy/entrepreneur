$('.gallery__photo-slider').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  asNavFor: '.gallery__slider',
  responsive: [
    {
      breakpoint: 767,
      settings: {
        dots: true
      }
    }
  ]
});

$('.gallery__slider').slick({
  slidesToShow: 8,
  slidesToScroll: 4,
  asNavFor: '.gallery__photo-slider',
  focusOnSelect: true,
  responsive: [
    {
      breakpoint: 1199,
      settings: {
        slidesToShow: 6,
        slidesToScroll: 3
      }
    },
    {
      breakpoint: 1023,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 2
      }
    }
  ]
});