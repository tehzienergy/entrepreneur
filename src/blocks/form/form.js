$('.form__item-input').change(function() {
  $(this).addClass('form__item-input--active');
  if( !$(this).val() ) {
    $(this).removeClass('form__item-input--active');
  }
});

$('.form__item--success .form__item-input').change();