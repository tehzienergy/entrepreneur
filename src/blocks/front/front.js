function headerPadding(element, additionalOffset) {
  $(element).css('padding-top', $('.header').outerHeight(true) + additionalOffset);
}

var additionalOffset = 50;

if ($(window).width() < 960) {
  additionalOffset = 20;
}

headerPadding('.slider__item', additionalOffset);

headerPadding('.front__right', additionalOffset);