$('.slider').slick({
  arrows: false,
  dots: true,
  autoplay: true,
  autoplaySpeed: 3000,
});

$('.slider').on('beforeChange', function(event, slick, currentSlide, nextSlide) {
  var slideNumber = $('.slick-dots li.slick-active').index() + 1;
  if (slideNumber == 5) {
    slideNumber = 0;
  }
  $('.front__slider-picture').removeClass('front__slider-picture--active');
  $('.front__slider-picture').eq(slideNumber).addClass('front__slider-picture--active');
});