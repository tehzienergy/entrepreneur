$('.select__input').each(function() {
  var select = $(this);
  select.select2({
    minimumResultsForSearch: -1,
    containerCssClass: 'select--form',
    placeholder: '',
    width: '100%',
    dropdownAutoWidth: true,
    dropdownCssClass: 'select__dropdown--common',
    dropdownParent: select.closest('.select')
  });
});

$('.select__input--region').each(function() {
  var select = $(this);
  select.select2({
    minimumResultsForSearch: -1,
    containerCssClass: 'select--btn',
    placeholder: 'Выберите регион',
    dropdownAutoWidth: true,
    dropdownCssClass: 'select__dropdown--common',
    dropdownParent: select.closest('.select')
  });
});